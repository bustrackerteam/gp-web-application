import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from'firebase';

var config= {
    apiKey: "AIzaSyBZB12HbOO-t9JLum4b8PL9d2ltKE58EvE",
    authDomain: "tracky-firebase-react-app.firebaseapp.com",
    databaseURL: "https://tracky-firebase-react-app.firebaseio.com",
    projectId: "tracky-firebase-react-app",
    storageBucket: "tracky-firebase-react-app.appspot.com",
    messagingSenderId: "111971666295"
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
